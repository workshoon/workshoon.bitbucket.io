var count = 0,
	zoneListFn = function(e){
		var $zl = $('#zoneList'),
			slick = $zl.find('.list');
		slick.slick({
			mobileFirst:true,
			arrows:false
		});
		$zl.swipe({
			swipeUp:function(e) {
				storeShowFn();
			}
		});
	},
	reservationListFn = function(e){
		var $rl = $('#reservationList'),
			slick = $rl.find('.list');
		slick.slick({
			mobileFirst:true,
			arrows:false
		});
	},
	showReservationList = function(){
		var $rl = $('#reservationList');
		if($rl.hasClass('hidden')){
			addBlockDim();
			$rl.removeClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$rl.off('transitionend webkitTransitionEnd oTransitionEnd');
				removeBlockDim();
			});
		};
	},
	hideReservationList = function(){
		var $rl = $('#reservationList');
		if(!$rl.hasClass('hidden')){
			addBlockDim();
			$rl.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$rl.off('transitionend webkitTransitionEnd oTransitionEnd');
				removeBlockDim();
			});
		};
	},
	showZoneList = function(){
		var $zl = $('#zoneList');
		if($zl.hasClass('hidden')){
			addBlockDim();
			$zl.removeClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$zl.off('transitionend webkitTransitionEnd oTransitionEnd');
				removeBlockDim();
			});
		};
	},
	hideZoneList = function(){
		var $zl = $('#zoneList');
		if(!$zl.hasClass('hidden')){
			addBlockDim();
			$zl.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$zl.off('transitionend webkitTransitionEnd oTransitionEnd');
				removeBlockDim();
			});
		};
	},
	addBlockDim = function(e){
		if($('.blockdim').length == 0){
			$('body').append('<div class="blockdim" />');
		};
	},
	removeBlockDim = function(e){
		$('.blockdim').remove();
	},
	storeShowFn = function(e){
		var $st = $('#store');
		addBlockDim();
		$st.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$st.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move hidden');
			removeBlockDim();
		});
		$(window).on('scroll.stations',function(e){
			var st = $(window).scrollTop();
			if(st>220){
				$st.addClass('topfix');
			}else{
				$st.removeClass('topfix');
			}
		});
	},
	storeHideFn = function(e){
		var $st = $('#store');
		addBlockDim();
		$st.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$st.off('transitionend webkitTransitionEnd oTransitionEnd');
			removeBlockDim();
		});
	},
	stationsShowFn = function(e){
		var $st = $('#stations');
		addBlockDim();
		$st.removeClass('hidden').addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$st.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('hidden');
			removeBlockDim();
		});
	},
	stationsHideFn = function(e){
		var $st = $('#stations');
		addBlockDim();
		$st.removeClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$st.off('transitionend webkitTransitionEnd oTransitionEnd').addClass('hidden').find('input').prop('checked',false);
			removeBlockDim();
		});
	},
	calenderListReservationFn = function(e){
		var $cl = $('#calenderListReservation'),
			$slick = $cl.find('.list');
		$slick.slick({
			mobileFirst:true,
			infinite:false
		});
	},
	calenderListReturnFn = function(e){
		var $cl = $('#calenderListReturn'),
			$slick = $cl.find('.list');
		$slick.slick({
			mobileFirst:true,
			infinite:false
		});
	},
	showLayer = function(obj,tar){
		var $layer = $(obj);
		$layer.removeClass('hide');
		if($layer.hasClass('set_transition')){
			var t = setTimeout(function(e){
				$layer.addClass('show');
			},100);
		};
		$layer.find('.btn_close').on('click',function(e){
			closeLayer(obj,tar);
		});
		if($layer.find('.time_picker').length == 1){
			$layer.data('tar',tar);
		};
	},
	closeLayer = function(obj,tar){
		var $layer = $(obj),
			checkLength = obj=='#layerReturnLength';
			timeLayerFn = function(e){
				var time = [];
				$layer.find('input.selected').each(function(e){
					time.push($(this).val());
				});
				$layer.find('.input_time').val(time.join(':'));
			};
		if($layer.find('.time_picker').length == 1){
			if(checkLength){
				$layer.find('.input_time').val(Number($layer.find('input.selected').val().split('대')[0]));
			}else{
				timeLayerFn();
			}
			var t = checkLength?$layer.find('.input_time').val()+'대':$layer.find('.input_time').val();
			$($layer.data('tar')).text(t);
			$layer.find('.btn_confirm').off('click');
		};
		if(obj == '#layerReturnTimeFinal' || obj == '#layerReturnLength'){
			var check1 = !$('#layerReturnTimeFinal .input_time').val(),
				check2 = !$('#layerReturnLength .input_time').val();
			if(!check1 && !check2){
				$('#return .payment_select').removeClass('hide');
				$('#return').stop().animate({scrollTop:$('#return .payment_select').offset().top},300,'swing');
			}
		}
		if($layer.hasClass('set_transition')){
			$layer.removeClass('show').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$layer.off('transitionend webkitTransitionEnd oTransitionEnd').addClass('hide');
			});
		}else{
			$layer.addClass('hide');
		}
		$layer.find('.btn_close').off('click');
	},
	customPicker = function(obj){
		var $this = $(obj),
			checkText = $this.find('select').attr('data-type')=='text';
		$this.find('option').each(function(index,obj){
			var val = checkText?$(obj).text():obj.value
			if(index==0){
				$this.append('<ul />');
			}
			if(obj.selected){
				$this.find('ul').append('<li><input type="text" class="input_picker selected" readonly value="'+val+'"></li>')
			}else{
				$this.find('ul').append('<li><input type="text" class="input_picker" readonly value="'+val+'"></li>')
			}
		});
		$this.find('select').addClass('hide');
		var $tar = $this.find('ul'),
			dis = 0,
			def = $this.find('.selected').closest('li').index()>0?-$this.find('.selected').closest('li').index()*64:0,
			lim = ($this.find('li').length-1)*64;
		$this.find('li').each(function(index,obj){
			$(this).attr('data-pos',index*64);
		})
		$tar.css('transform','translateY('+def+'px)').swipe({
			swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection){
				if(phase == 'move' && (direction=='up' || direction=='down')){
					if(direction=='up'){
						dis = -distance+def;
					}else if(direction=='down'){
						dis = distance+def;
					}
					if(dis<-lim){
						dis = -lim;
					}
					if(dis>0){
						dis = 0;
					}
					$tar.css('transform','translateY('+dis+'px)');
				}
				if(phase == 'end' || phase == 'cancel'){
					def = dis;
					$tar.find('.selected').removeClass('selected');
					$tar.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
						$tar.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move');
					}).find('li').eq(Math.floor(-dis/64)).find('.input_picker').addClass('selected');
					var t = -$tar.find('li').eq(Math.floor(-dis/64)).attr('data-pos');
					$tar.css('transform','translateY('+t+'px)');
					if(dis==-lim||dis==0){
						$tar.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move');
					}
				}
			}
		})
	},
	reservationShowFn = function(e){
		var $re = $('#reservation');
		addBlockDim();
		$re.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$re.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move hidden');
			$re.find('.step1').removeClass('hidden');
			removeBlockDim();
		}).find('.step1').removeClass('hidden');
	},
	reservationHideFn = function(e){
		var $re = $('#reservation');
		addBlockDim();
		$re.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$re.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('s2 s3 s4').scrollTop(0).find('>.inner >div').addClass('hidden');
			reservationReset();
			removeBlockDim();
		});
	},
	reservationReset = function(e){
		var $re = $('#reservation'),
			$reS1 = $re.find('.step1'),
			$reS2 = $re.find('.step2'),
			$reS3 = $re.find('.step3'),
			$reS4 = $re.find('.step4');
		$('#layerRentalTime .input_time').val('');
		$('#layerReturnTime .input_time').val('');
		$('#layerPoint').find('input[type="number"]').val(null).end()
		.find('input[type="checkbox"]').prop('checked',0);
		$reS1.find('.set_time').find('button').eq(0).text('대여시간').end().eq(1).text('반납시간');
		$re.off('scroll.reservationS2');
		$reS2.find('.btn_top').off('click.reservationS2').end()
		.find('.header').removeClass('fix').end()
		.find('.btn_top').removeClass('show');
		$reS3.find('.more').removeClass('hide').end()
		.find('.product_detail, .product_notice, .product_terms').addClass('default').end()
		.find('.return_point,.return_time').removeClass('open').end()
		.find('.input_length').val(0).prop('disabled','disabled').end()
		.find('.product_info .input_length').removeAttr('disabled').val(1).end()
		.find('button.minus').prop('disabled','disabled').end()
		.find('select#returnPoint').prop('selectedIndex','-1').end()
		.find('.return_point input[type="text"]').val(null).end()
		.find('.return_time input[type="text"]').val(null);
		$reS4.find('.reservation_info textarea').removeAttr('style').val(null).end()
		.find('.custom_check input[type="checkbox"]').prop('checked',0).end()
		.find('.terms_check .detail').removeClass('open').end()
		.find('.terms_check .conts').scrollTop(0).end()
		.find('.btn_allview').removeData('open');
	},
	reservationS2Fn = function(e){
		var $re = $('#reservation'),
			$reS2 = $re.find('.step2');
		$re.on('scroll.reservationS2',function(e){
			var st = $re.scrollTop();
			if(st > 61){
				$reS2.find('.header').addClass('fix');
				$reS2.find('.btn_top').addClass('show');

			}else{
				$reS2.find('.header').removeClass('fix');
				$reS2.find('.btn_top').removeClass('show');
			}
		});
		$reS2.find('.btn_top').on('click.reservationS2',function(e){
			$re.stop().animate({scrollTop:0},300,'swing');
		});
	},
	reservationS3Fn = function(e){
		var $re = $('#reservation'),
			$reS3 = $re.find('.step3');
		$reS3.find('.more').each(function(i,obj){
			var $tar = $(obj).prev('.default');
			$tar.removeClass('default').attr('data-originheight',$tar.height()).addClass('default');
		});
	},
	reservationS4Fn = function(e){
		var $re = $('#reservation'),
			$reS4 = $re.find('.step4 .reservation_info');
	},
	reservationNextFn = function(step){
		var $re = $('#reservation'),
			$reS1 = $re.find('.step1'),
			$reS2 = $re.find('.step2'),
			$reS3 = $re.find('.step3'),
			$reS4 = $re.find('.step4'),
			st = 's'+step;
		addBlockDim();
		if(step == 2){
			reservationS2Fn();
		}else if(step == 3){
			$re.off('scroll.reservationS2');
			$reS2.find('.btn_top').off('click.reservationS2').removeClass('show').end().find('.header').removeClass('fix');
			reservationS3Fn();
			productTotal();
		}else if(step == 4){
			reservationS4Fn();
		};
		$re.removeClass('s1 s2 s3 s4').stop().animate({scrollTop:0},300,'swing').addClass(st).on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$re.off('transitionend webkitTransitionEnd oTransitionEnd');
			$re.find('.step'+(step-1)).addClass('hidden');
			removeBlockDim();
		}).find('>.inner >div').eq(step-1).removeClass('hidden');
	},
	reservationPrevFn = function(step){
		var $re = $('#reservation'),
			$reS1 = $re.find('.step1'),
			$reS2 = $re.find('.step2'),
			$reS3 = $re.find('.step3'),
			$reS4 = $re.find('.step4'),
			st = 's'+step;
		addBlockDim();
		if(step == 1){
			$re.off('scroll.reservationS2');
			$reS2.find('.btn_top').off('click.reservationS2');
			$reS2.find('.header').removeClass('fix');
			$reS2.find('.btn_top').removeClass('show');
		}else if(step == 2){
			reservationS2Fn();
		};
		$re.stop().animate({scrollTop:0},300,'swing').removeClass('s1 s2 s3 s4').addClass(st).on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$re.off('transitionend webkitTransitionEnd oTransitionEnd');
			$re.find('.step'+(step+1)).addClass('hidden');
			removeBlockDim();
		}).find('>.inner >div').eq(step-1).removeClass('hidden');;
	},
	detailImgListFn = function(e){
		var $dl = $('#detailImgList'),
			$slick = $dl.find('.list');
		$slick.slick({
			mobileFirst:true,
			arrows:false,
			dots:true
		});
	},
	productCalFn = function(type,obj){
		var $tar = obj.closest('.option'),
			$total = $('#total'),
			checkIf = !obj.closest('div').find('.input_length').prop('min'),
			limitMin = checkIf?0:Number(obj.closest('div').find('.input_length').prop('min')),
			nowValue = $tar.find('.input_length').val();
		if(type=='plus'){
			nowValue++;
		}else{
			nowValue--;
		}
		if(nowValue == limitMin){
			nowValue == limitMin;
			$tar.find('.minus').prop('disabled','disabled');
			if(checkIf){
				$tar.find('.input_length').prop('disabled','disabled');
			}
			if($tar.find('div:last-child').is('.return_time')){
				$tar.find('.return_time').removeClass('open').find('input').val('').prop('disabled','disabled');
			}
			if($tar.find('div:last-child').is('.return_point')){
				$tar.find('.return_point').removeClass('open').find('input').val('').prop('disabled','disabled');
			}
		}
		$tar.find('.input_length').prop('value',nowValue);
		if(nowValue > limitMin){
			$tar.find('.minus').removeAttr('disabled');
			$tar.find('.input_length').removeAttr('disabled');
			if($tar.find('div:last-child').is('.return_time')){
				$tar.find('.return_time').addClass('open').find('input').removeAttr('disabled');
			}
			if($tar.find('div:last-child').is('.return_point')){
				$tar.find('.return_point').addClass('open').find('input').removeAttr('disabled');
				$tar.find('select').prop('selectedIndex','-1');
			}
		}
		productTotal();
	},
	productTotal = function(e){
		var price = $('.product_info .price input').val() * $('.product_info .length .input_length').val(),
			option = 0;
		$('.product_option .options').each(function(e){
			option = option + ($(this).find('.price input').val() * $(this).find('.length .input_length').val());
		});
		var t = price+option;
		$('#total').text(t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	},
	termsAllview = function(e){
		var $re = $('#reservation'),
			$step4 = $re.find('.step4'),
			$tar = $step4.find('.terms_check .detail');
		if(!$step4.find('.btn_allview').data('open')){
			$step4.find('.btn_allview').data('open',true);
			$tar.addClass('open').find('a').on('click',function(e){
				e.preventDefault();
				var scroll = $tar.find('.conts'),
					v = $($(this).attr('href')).position().top-5;
				scroll.stop().animate({scrollTop:v},300,'swing');
			});
			$re.stop().animate({scrollTop:$tar.offset().top+214},300,'swing');
		}else{
			$step4.find('.btn_allview').data('open',false);
			$re.stop().animate({scrollTop:$tar.offset().top+214},300,'swing',function(e){
				$tar.removeClass('open').find('a').off('click');
				$tar.find('.conts').scrollTop(0);
			});
		}
	},
	returnShowFn = function(e){
		var $re = $('#return');
		addBlockDim();
		$re.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$re.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move hidden');
			$re.find('.step1').removeClass('hidden');
			removeBlockDim();
		});
	},
	returnHideFn = function(e){
		var $re = $('#return');
		addBlockDim();
		$re.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$re.off('transitionend webkitTransitionEnd oTransitionEnd').scrollTop(0);
			returnReset();
			removeBlockDim();
		}).find('>.inner >div').addClass('hidden');
	},
	returnReset = function(e){
		var $re = $('#return');
		$re.find('.payment_select').addClass('hide');
		$re.find('.set_time button').eq(0).text('반납시간');
		$re.find('.set_time button').eq(1).text('반납대수');
		$('#layerReturnTimeFinal .input_time').val('');
		$('#layerReturnLength .input_time').val('');
	},
	showSlideinFn = function(obj){
		var $this = $(obj);
		addBlockDim();
		$this.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$this.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move before');
			removeBlockDim();
		});
	},
	hideSlideinFn = function(obj){
		var $this = $(obj);
		addBlockDim();
		$this.addClass('before').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$this.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('set_transition');
			var time = setTimeout(function(e){
				resetJoinForm();
				$this.addClass('set_transition');
				removeBlockDim();
			},350);
		});
	},
	nextSlideinFn = function(obj,obj2){
		var $now = $(obj2).closest('.pos_over'),
			$next = $(obj);
		addBlockDim();
		$now.addClass('after').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$now.off('transitionend webkitTransitionEnd oTransitionEnd');
		});
		$next.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$next.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move before');
			removeBlockDim();
		});
	},
	prevSlideinFn = function(obj,obj2){
		var $now = $(obj2).closest('.pos_over'),
			$prev = $(obj);
		addBlockDim();
		$now.addClass('before').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$now.off('transitionend webkitTransitionEnd oTransitionEnd');
		});
		$prev.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$prev.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move after');;
			removeBlockDim();
		});
	},
	resetJoinForm = function(e){
		var $jf = $('#joinForm');
		$jf.addClass('before').find('.pos_over').removeClass('after before set_transition').addClass('before').eq(0).removeClass('before');
		$jf.find('.error').addClass('hide');
		var t = setTimeout(function(e){
			clearTimeout(t);
			$jf.find('.pos_over').addClass('set_transition');
		},350);
	},
	showLayerMessage = function(msg){
		var layer = $('<div class="layer_message set_transition hidden" />');
		layer.append('<p>'+msg+'</p>');
		$('body').append(layer);
		var t = setTimeout(function(e){
			clearTimeout(t);
			layer.removeClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				layer.off('transitionend webkitTransitionEnd oTransitionEnd');
			});
		},500);
		var t2 = setTimeout(function(e){
			clearTimeout(t2);
			layer.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				layer.off('transitionend webkitTransitionEnd oTransitionEnd');
			});
		},4000);
	},
	showIntro = function(obj,delay,duration){
		var $this = $(obj);
		$this.removeClass('hide')
		var t = setTimeout(function(){
			clearTimeout(t);
			$this.css({
				'-webkit-transition-delay':delay+'s',
				'-o-transition-delay':delay+'s',
				'transition-delay':delay+'s',
				'-webkit-transition-duration':duration+'s',
				'-o-transition-duration':duration+'s',
				'transition-duration':duration+'s'
			}).addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$this.off('transitionend webkitTransitionEnd oTransitionEnd').addClass('hide');
			});
		},100);
	},
	allResetFn = function(e){
		
	},
	homeMenuFn = function(obj){
		$('#wrap .home_menu').find('.on').removeClass('on');
		obj.addClass('on');
	},
	showLevelup = function(l,time){
		var cn,txt;
		if(l == 5){
			cn = 'l5';
			txt = '<em>지구</em>가';
		}else if(l == 4){
			cn = 'l4';
			txt = '<em>숲</em>이';
		}else if(l == 3){
			cn = 'l3';
			txt = '<em>나무</em>가';
		}else{
			cn = 'l2';
			txt = '<em>새싹</em>이';
		};
		$('body').append('<div class="layer_wrap layer_levelup set_transition '+cn+'" id="layerLevelup"><p class="bg_before pos_center">친환경 이동 수단으로 에너지를 절약하여<br>'+txt+' 되었습니다.</p></div>');
		var t = setTimeout(function(e){
			$('#layerLevelup').addClass('show');
			clearTimeout(t);
			var t = setTimeout(function(e){
				$('#layerLevelup').removeClass('show').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
					$(this).off('transitionend webkitTransitionEnd oTransitionEnd').remove();
					clearTimeout(t);
				});
			},time)
		},300);
	},
	showMypage = function(e){
		var $mp = $('#mypage');
		if($mp.hasClass('hidden')){
			addBlockDim();
			$mp.removeClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$(this).off('transitionend webkitTransitionEnd oTransitionEnd');
				removeBlockDim();
			});
		}
	},
	hideMypage = function(e){
		var $mp = $('#mypage');
		if(!$mp.hasClass('hidden')){
			addBlockDim();
			$mp.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$(this).off('transitionend webkitTransitionEnd oTransitionEnd');
				removeBlockDim();
				resetMypage();
			});
		}
	},
	homeMenuAllHide = function(obj){
		if(!$(obj).hasClass('on')){
			hideReservationList();
			hideZoneList();
			hideMypage();
		};
	},
	mypageNext = function(obj,prev){
		var $obj = $(obj),
			$mp = $('#mypage');
		addBlockDim();
		$('html,body').stop().animate({scrollTop:0},300,'swing');
		$obj.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$(this).off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('hidden move');
			if($(prev).closest('.step1').is('div') && $obj.height()<$(prev).closest('.step1').height()){
				$(prev).closest('.step1').attr('data-origin',true).addClass('cut');
			}else{
				if($(prev).closest('.sub_mypage').is('div') && $obj.height()<$(prev).closest('.sub_mypage').height()){
					$(prev).closest('.sub_mypage').attr('data-origin',true).addClass('cut');
				};
			};
			$mp.find('.mypage_main').addClass('hidden');
			removeBlockDim();
			$(window).on({
				'scroll.mypage':function(e){
					var st = $(window).scrollTop();
					if(st>15){
						$obj.find('.header').addClass('fix');
					}else{
						$obj.find('.header').removeClass('fix');
					}
				}
			})
		});
		if(obj.indexOf('step2')>0){
			var tar = $obj.closest('div.sub_mypage').hasClass('my_reservation')?$obj.closest('div.my_reservation'):$obj.closest('div.my_event');
			tar.find('.step1').addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$(this).off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move').addClass('hidden');
			});
		}
	},
	mypagePrev = function(obj){
		var $obj = $(obj);
		addBlockDim();
		$('html,body').stop().animate({scrollTop:0},300,'swing');
		$obj.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$(this).off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('move').addClass('hidden');
			var t = obj.indexOf('step2')>0?$('div.step1[data-origin]'):$('div.sub_mypage[data-origin]');
			t.removeClass('cut').removeAttr('data-origin');
			if(obj.indexOf('step2')<0){
				reservationTypeReset();
			};
			removeBlockDim();
		});
		if(obj.indexOf('step2')>0){
			var tar = $obj.closest('div.sub_mypage').hasClass('my_reservation')?$obj.closest('div.my_reservation'):$obj.closest('div.my_event');
			tar.find('.step1').removeClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
				$(this).off('transitionend webkitTransitionEnd oTransitionEnd');
			});
		}else{
			$('#mypage .mypage_main').removeClass('hidden');
		};
	},
	resetMypage = function(e){
		var $mp = $('#mypage');
		$(window).off('scroll.mypage');
		$mp.find('.mypage_main').removeClass('hidden').end()
			.find('.header').removeClass('fix');
		$mp.find('>.set_transition').removeClass('set_transition move').addClass('hidden set_transition').end()
			.find('.header').removeClass('fix').end()
			.find('.step1').removeClass('move hidden').end()
			.find('.step2').removeClass('move').addClass('hidden').end()
			.find('.list_qna .q .btn_qna').addClass('close').end();
		reservationTypeReset();
	},
	reservationType = function(obj){
		var $mr = $('#mypageReservation'),
			$list = $(obj).closest('.list_type'),
			txt = '전체보기',
			v = obj.value;
		$list.find('button').removeClass('sel');
		$(obj).addClass('sel');
		if(v == 'ing'){
			txt = '이용중'
		}else if(v == 'wait'){
			txt = '신청대기'
		}else if(v == 'complete'){
			txt = '신청완료'
		}else if(v == 'cancel'){
			txt = '예약취소'
		}else if(v == 'done'){
			txt = '이용완료'
		}else if(v == 'noshow'){
			txt = '노쇼'
		};
		$mr.find('.btn_viewmenu').text(txt);
		$mr.find('.list_reservation li').removeClass('hide');
		if(v!='all'){
			$mr.find('.list_reservation li').filter(function(index) {
				var r = $(this).hasClass(v)?null:$(this);
				return r;
			}).addClass('hide');
		}
		closeLayer('#layerReservationtype');
	},
	reservationTypeReset = function(e){
		var $mp = $('#mypage'),
			$mr = $('#mypageReservation');
		$mr.find('.btn_viewmenu').text('전체보기').end()
		.find('.list_reservation li').removeClass('hide').end()
		.find('.btn_more').removeClass('hide');
		$('#layerReservationtype .list_type button').removeClass('sel').eq(0).addClass('sel');
	},
	gnaFn = function(obj){
		var $q = obj,
			$a = obj.closest('.item').find('.a'),
			h = $a.find('.inner').outerHeight(),
			closeFn = function(a){
				a.addClass('close return').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
					a.off('transitionend webkitTransitionEnd oTransitionEnd').find('.bg').removeClass('set_transition').end().removeClass('return');
					var t = setTimeout(function(e){
						clearTimeout(t);
						a.find('.bg').addClass('set_transition')
					},100);
				}).closest('.item').find('.a').height(0);
			};
		if($q.hasClass('close')){
			obj.closest('.list_qna').find('.btn_qna').each(function(index){
				var $this = $(this);
				if(!$this.hasClass('close')){
					closeFn($this);
				}
			});
			$q.removeClass('close');
			$a.height(h);
		}else{
			closeFn($q);
		};
	},
	autolineFn = function(obj){
		obj.height(1).height(8+obj[0].scrollHeight);
	},
	showProductList = function(){
		var $pl = $('#productList');
		addBlockDim();
		$pl.addClass('move').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$pl.off('transitionend webkitTransitionEnd oTransitionEnd').removeClass('hidden move');
			removeBlockDim();
		});
	},
	prevProductList = function(){
		var $pl = $('#productList');
		addBlockDim();
		$pl.addClass('hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$pl.off('transitionend webkitTransitionEnd oTransitionEnd');
			removeBlockDim();
		});
	},
	hideProductList = function(){
		var $pl = $('#productList');
		addBlockDim();
		$pl.addClass('top hidden').on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
			$pl.off('transitionend webkitTransitionEnd oTransitionEnd');
			removeBlockDim();
		});
	},
	focusinFn = function(obj){
		if(obj.is('input[type="text"]') || obj.is('input[type="password"]') || obj.is('input[type="email"]') || obj.is('input[type="number"]') || obj.is('#reservationEtc')){
			$('#loginForm, #joinForm, #findIdForm, #findPwForm, #reservation .step4').removeAttr('style');
			var a = Number($('body').data('h')),
				b = $(window).height();
			$('#loginForm, #joinForm, #findIdForm, #findPwForm, #reservation .step4').addClass('keyboard').height(b+(a-b));
			$('#wrap >.footer').addClass('hide');
		}
	}
	focusoutFn = function(e){
		$('.keyboard').removeAttr('style').removeClass('keyboard');
		$('#loginForm, #joinForm, #findIdForm, #findPwForm, #reservation .step4').removeAttr('style');
		$('#wrap >.footer').removeClass('hide');
	};
$(document).ready(function($){
	window.addEventListener("resize", function() {
		if(document.activeElement.tagName=="INPUT" || document.activeElement.tagName=="TEXTAREA") {
			document.activeElement.scrollIntoView();
		}
	});
	$('body').data('h',$(window).height()).on({
		click:function(e){
			var $this = $(e.target);
			/*if($this.closest('button').hasClass('btn') && $this.closest('.zone_list').is('#zoneList')){
				storeShowFn();
			}*/
			if($this.closest('button').hasClass('btn_back') && $this.closest('.store').is('#store')){
				storeHideFn();
			};
			if($this.is('button') && $this.is('#btnShowStations')){
				stationsShowFn();
			};
			if($this.closest('button').hasClass('btn_close') && $this.closest('.reservation').is('#reservation')){
				reservationHideFn();
			};
			if($this.closest('button').hasClass('btn_close') && $this.closest('.return').is('#return')){
				returnHideFn();
			};
			if($this.closest('button').hasClass('btn_more') && $this.closest('.reservation').is('#reservation')){
				var $tar = $this.closest('.more').prev('.default');
				$tar.css('height',$tar.attr('data-originheight')).on('transitionend webkitTransitionEnd oTransitionEnd',function(e){
					$tar.removeClass('default').removeAttr('style data-originheight');
					$this.closest('.more').addClass('hide');
				});
			};
			if($this.closest('button').hasClass('plus') && $this.closest('div').hasClass('length')){
				productCalFn('plus',$this);
			};
			if($this.closest('button').hasClass('minus') && $this.closest('div').hasClass('length')){
				productCalFn('minus',$this);
			};
			if($this.closest('button').hasClass('btn_allview') && $this.closest('.terms_check').length == 1){
				termsAllview();
			};
			if($this.closest('button').hasClass('link') && $this.closest('nav').hasClass('home_menu')){
				homeMenuFn($this);
			};
			if($this.closest('button').hasClass('btn_qna') && $this.closest('.list_qna').is('div')){
				gnaFn($this);
			};
		},
		change:function(e){
			var $this = $(e.target);
			if($this.closest('.stations').is('#stations')){
				var $st = $('#stations');
				if($this.is('#checkAll')){
					if($this.is(':checked')){
						$st.find('.map input').prop('checked',true);
					}else{
						$st.find('.map input').prop('checked',false);
					}
				}
				if($this.hasClass('pos_center')){
					if($st.find('.map input:checked').length==4){
						$st.find('.custom_check input').prop('checked',true);
					}else{
						$st.find('.custom_check input').prop('checked',false);
					}
					$this.closest('.map').find('input[type=checkbox]').filter(function(index) {
						var r;
						if(this.id!=$this[0].id){
							r = $(this)
						}
						return r;
					}).prop('checked',false);
				}
			}
			if($this.is('#returnPoint')){
				$this.prev('input').val($this.val());
			}
		},
		keyup:function(e){
			var $this = $(e.target);
			if($this.is('textarea.autoline')){
				autolineFn($this)
			}
		},
		keydown:function(e){
			var $this = $(e.target);
			if($this.is('textarea.autoline')){
				autolineFn($this)
			}
		},
		focusin:function(e){
			var $this = $(e.target);
			focusinFn($this);
		},
		focusout:function(e){
			focusoutFn();
		}
	});

	$(window).on('resize',function(e){
		var a = Number($('body').data('h')),
			b = $(window).height();
		$('.keyboard').find('.main').css({
			'box-sizing':'content-box',
			'height':b+(a-b)
		});
		if($('body').data('h') == $(window).height()){
			focusoutFn();
		}
		console.log( $('body').data('h'), $(window).height() );
	});
});